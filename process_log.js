const fs = require('fs');
const {loclass} = require('./index');

const DECIMAL = 10;

const captureRegex =
  /loclass-v1-info ts \d+ started(?<logs>[\s\S]+?)loclass-v1-info ts \d+ finished/g;

function parseLog(log) {
  console.log("parse log", log.length);
  const expectedLineCount = 2 + 2 * 9;
  var logSet = log;

  const matches = log.match(captureRegex);
  if (!matches || matches.length === 0) {
    console.error(`No set of logs found`);
    return;
  }
  if (matches.length > 1) {
    const candidates = matches.filter((log) => {
      const lines = log.split("\n").filter((l) => l);
      return lines.length === expectedLineCount;
    });
    if (candidates.length === 0) {
      console.error(
        `Multiple sets of logs detected, but none are ${expectedLineCount} lines long`
      );
      return;
    } else {
      console.error(
        `Multiple sets of logs detected.  Using most recent with correct length`
      );
      logSet = candidates[candidates.length - 1];
    }
  }

  const lines = logSet.split("\n").filter((l) => l);
  const lastIndex = lines.length - 1;

  if (logSet.match(captureRegex).length > 1) {
    console.error(
      "Multiple sets of logs detected: remove all but one set of logs"
    );
    return;
  }

  // 9 lines per key, running twice for keyrollcheck, plus start and end lines
  if (lines.length !== 2 + 2 * 9) {
    console.error(`Too many of too few lines: ${lines.length} should be 18`);
    return;
  }

  if (!lines[0].endsWith("started")) {
    console.error("Incorrect start line");
    return;
  }

  if (!lines[lastIndex].endsWith("finished")) {
    console.error("Incorrect end line");
    return;
  }

  let dataA = "";
  let dataB = "";

  // loclass-mac ts 1686142770 no 17 csn d25a82f8f7ff12e0 cc d2ffffffffffffff nr 00000000 mac 00000000
  lines.slice(1, lastIndex).forEach((line) => {
    const parts = line.split(" ");
    if (!parts[0].startsWith("loclass-v1-mac")) {
      setError("Unexpected log format");
      return;
    }
    let logLine = "";
    logLine += parts[6];
    logLine += parts[8];
    logLine += parts[10];
    logLine += parts[12];

    const no = parseInt(parts[4], DECIMAL);
    if (no % 2 === 0) {
      dataA += logLine;
    } else {
      dataB += logLine;
    }
  });
  if (dataA.length > 0) {
    calculateKey(Buffer.from(dataA, 'hex'));
  }
  if (dataB.length > 0) {
    calculateKey(Buffer.from(dataB, 'hex'));
  }
}


function calculateKey(iclassDump) {

  try {
    const start = Date.now();
    const key = loclass(iclassDump, iclassDump.length);
    console.log({ key, duration: Date.now() - start });
  } catch (e) {
    console.log('loclass failed', e);
  }
}

if (process.argv.length === 2) {
  console.error('Expected at least one argument!');
  process.exit(1);
}

const log = fs.readFileSync(process.argv[2], { encoding: 'utf8'} );
parseLog(log);
