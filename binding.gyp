{
  "targets": [
    {
      "target_name": "loclass",
      "include_dirs" : [
        "<!(node -e \"require('nan')\")",
        "loclass",
        "mbedtls/include"
      ],
      "sources": [
        "loclass/elite_crack.c",
        "loclass/optimized_cipher.c",
        "loclass/optimized_cipherutils.c",
        "loclass/optimized_elite.c",
        "loclass/optimized_ikeys.c",
        "loclass/pm3.c",
        "mbedtls/library/des.c",
        "mbedtls/library/platform_util.c",
        "main.cc"
      ],
      "libraries": [
      ],
      "link_settings": {
        "libraries": [
        ]
      }
    }
  ]
}


