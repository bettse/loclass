const fs = require('fs');

const {loclass, diversifyKey, opt_doTagMAC, opt_doReaderMAC} = require('./index');

const known = Buffer.from('5B7C62C491C11B39', 'hex');
const standard_key = Buffer.from('afa785a7dab33378', 'hex');

function testLoclass() {
  const iclassDump = fs.readFileSync('./iclass_dump.bin');

  try {
    const key = loclass(iclassDump, iclassDump.length);
    if (key.equals(known)) {
      console.log('SUCCESS');
    } else {
      console.log('MISMATCH', {known: known.toString('hex'), key: key.toString('hex')});
    }
  } catch (e) {
    console.log('loclass failed', e);
  }
}

function testDiversifyKey() {
  // Standard key diversification
  try {
    const csn = Buffer.from('285F9D00F9FF12E0', 'hex');
    const known = Buffer.from('725EE635ADF721FE', 'hex')
    const div_key = diversifyKey(csn, standard_key);
    if (div_key.equals(known)) {
      console.log('SUCCESS');
    } else {
      console.log('MISMATCH', {known: known.toString('hex'), div_key: div_key.toString('hex')});
    }
  } catch (e) {
    console.log('diversifyKey failed', e);
  }
}

function testDiversifyKeyElite() {
  // Elite key diversification
  try {
    const elite = true;
    const key = Buffer.from('7D52F3F5F4BF1846', 'hex');
    const csn = Buffer.from('89000412FEFF12E0', 'hex');
    const known = Buffer.from('1BA67B5664C3E9BA', 'hex')
    const div_key = diversifyKey(csn, key, elite);
    if (div_key.equals(known)) {
      console.log('SUCCESS');
    } else {
      console.log('MISMATCH', {known: known.toString('hex'), div_key: div_key.toString('hex')});
    }
  } catch (e) {
    console.log('diversifyKey failed', e);
  }
}

function testOpt_doTagMAC() {
  try {
    const csn = Buffer.from('F87CD712FFFF12E0', 'hex');
    const div_key = diversifyKey(csn, standard_key);

    const epurse = Buffer.from('FFFFFFFFE3FFFFFF', 'hex');
    const cc = Buffer.from('DEF88EE8F0CEE20F', 'hex');
    const known = Buffer.from('8DCAC12A', 'hex');

    const cc_p = Buffer.concat([epurse, cc.slice(0, 4)]);
    const tmac = opt_doTagMAC(cc_p, div_key);

    if (tmac.equals(known)) {
      console.log('SUCCESS');
    } else {
      console.log('MISMATCH', {known: known.toString('hex'), tmac: tmac.toString('hex')});
    }
  } catch (e) {
    console.log('opt_doTagMAC failed', e);
  }
}

function testOpt_doReaderMAC() {
  try {
    const csn = Buffer.from('4BDEBC03F9FF12E0', 'hex');
    const div_key = diversifyKey(csn, standard_key);

    const cc = Buffer.from('E0FFFFFFFFFFFFFF', 'hex');
    const nr = Buffer.from('9D9A50C0', 'hex');
    const known = Buffer.from('F35C58FD', 'hex');

    const cc_nr_p = Buffer.concat([cc, nr]);
    const rmac = opt_doReaderMAC(cc_nr_p, div_key);

    if (rmac.equals(known)) {
      console.log('SUCCESS');
    } else {
      console.log('MISMATCH', {known: known.toString('hex'), rmac: rmac.toString('hex')});
    }
  } catch (e) {
    console.log('opt_doReaderMAC failed', e);
  }
}

console.log('Test opt_doReaderMAC');
testOpt_doReaderMAC();

console.log('Test opt_doTagMAC');
testOpt_doTagMAC();

console.log('Test diversifyKey');
testDiversifyKey();

console.log('Test diversifyKey (elite)');
testDiversifyKeyElite();



console.log('Test loclass');
testLoclass();
