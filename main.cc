#include <nan.h>

#define __STDC_FORMAT_MACROS
#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C"
{
#endif

#include "loclass/elite_crack.h"
#include "loclass/optimized_cipher.h"
#include "loclass/optimized_cipherutils.h"
#include "loclass/optimized_elite.h"
#include "loclass/optimized_ikeys.h"
#include "loclass/pm3.h"

#ifdef __cplusplus
} // extern "C"
#endif

using v8::Context;
using v8::Exception;
using v8::FunctionCallbackInfo;
using v8::Isolate;
using v8::Local;
using v8::Object;
using v8::String;
using v8::Number;
using v8::Value;
using v8::Function;

NAN_METHOD(loclass) {
  Isolate* isolate = info.GetIsolate();
  Local<Context> context = isolate->GetCurrentContext();

  if (!info[0]->IsObject()) {
    Nan::ThrowError("First argument must be a buffer");
    return;
  }

  if (!info[1]->IsNumber()) {
    Nan::ThrowError("Second argument must be a number");
    return;
  }

  char* buffer = (char*) node::Buffer::Data(info[0]->ToObject(context).ToLocalChecked());
  uint32_t size = info[1]->Uint32Value(context).ToChecked();

  if(size % 24 != 0) {
    Nan::ThrowError("Size must be a multiple of 24");
    return;
  }

  uint16_t keytable[128] = {0};
  int res = bruteforceDump((uint8_t*)buffer, size, keytable);
  if (res != PM3_SUCCESS) {
    Nan::ThrowError("Failed to bruteforce");
  }

  uint8_t first16bytes[16] = {0};
  for (size_t i = 0 ; i < 16 ; i++) {
    first16bytes[i] = keytable[i] & 0xFF;
  }

  char* retval = new char[8];
  calculateMasterKey(first16bytes, (uint8_t*)retval);
  auto result = Nan::NewBuffer(retval, 8).ToLocalChecked();
  info.GetReturnValue().Set(result);
}

NAN_METHOD(diversifyKey) {
  Isolate* isolate = info.GetIsolate();
  Local<Context> context = isolate->GetCurrentContext();
  bool elite = false;

  if (!info[0]->IsObject()) {
    Nan::ThrowError("First argument must be a buffer");
    return;
  }

  if (!info[1]->IsObject()) {
    Nan::ThrowError("Second argument must be a buffer");
    return;
  }

  char* csn = (char*) node::Buffer::Data(info[0]->ToObject(context).ToLocalChecked());
  char* key = (char*) node::Buffer::Data(info[1]->ToObject(context).ToLocalChecked());
  if (info[2]->IsBoolean()) {
    elite = info[2]->BooleanValue(isolate);
  }

  char* div_key = new char[8];
  iclass_calc_div_key((uint8_t*)csn, (uint8_t*)key, (uint8_t*)div_key, elite);

  auto result = Nan::NewBuffer(div_key, 8).ToLocalChecked();
  info.GetReturnValue().Set(result);
}

NAN_METHOD(opt_doTagMAC) {
  Isolate* isolate = info.GetIsolate();
  Local<Context> context = isolate->GetCurrentContext();

  if (!info[0]->IsObject()) {
    Nan::ThrowError("First argument must be a buffer");
    return;
  }
  if (!info[1]->IsObject()) {
    Nan::ThrowError("Second argument must be a buffer");
    return;
  }

  char* cc_p = (char*) node::Buffer::Data(info[0]->ToObject(context).ToLocalChecked());
  char* div_key = (char*) node::Buffer::Data(info[1]->ToObject(context).ToLocalChecked());
  char* tmac = new char[4];

  opt_doTagMAC((uint8_t*)cc_p, (uint8_t*)div_key, (uint8_t*)tmac);

  auto result = Nan::NewBuffer(tmac, 4).ToLocalChecked();
  info.GetReturnValue().Set(result);
}


NAN_METHOD(opt_doReaderMAC) {
  Isolate* isolate = info.GetIsolate();
  Local<Context> context = isolate->GetCurrentContext();

  if (!info[0]->IsObject()) {
    Nan::ThrowError("First argument must be a buffer");
    return;
  }
  if (!info[1]->IsObject()) {
    Nan::ThrowError("Second argument must be a buffer");
    return;
  }

  char* cc_nr_p = (char*) node::Buffer::Data(info[0]->ToObject(context).ToLocalChecked());
  char* div_key = (char*) node::Buffer::Data(info[1]->ToObject(context).ToLocalChecked());
  char* tmac = new char[4];

  opt_doReaderMAC((uint8_t*)cc_nr_p, (uint8_t*)div_key, (uint8_t*)tmac);

  auto result = Nan::NewBuffer(tmac, 4).ToLocalChecked();
  info.GetReturnValue().Set(result);
}

NAN_MODULE_INIT(Initialize) {
  NAN_EXPORT(target, loclass);
  NAN_EXPORT(target, diversifyKey);
  NAN_EXPORT(target, opt_doTagMAC);
  NAN_EXPORT(target, opt_doReaderMAC);
}

NODE_MODULE(loclass, Initialize)
